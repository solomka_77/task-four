package hometask.other;

public abstract class Figures {
    private int height;
    private int base;

    public Figures (int height, int base) {
        this.height = height;
        this.base = base;
    }
    public double getHeight() {
        return height;
    }
    public double getbase() {
        return base;
    }
    public abstract void getArea();
}

