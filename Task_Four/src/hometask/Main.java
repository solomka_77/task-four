package hometask;

import hometask.other.Rhombus;
import hometask.other.Triangle;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
            System.out.print("Введите высоту: ");
                int height = in.nextInt();
            System.out.print("Введите сторону к которой опущена высота: ");
                int base = in.nextInt();

        Triangle triangle = new Triangle(height,base);
            System.out.print("Площадь треугольника = ");
        triangle.getArea();

        Rhombus rhombus = new Rhombus(height,base);
            System.out.print("Площадь ромба = ");
        rhombus.getArea();
    }
}